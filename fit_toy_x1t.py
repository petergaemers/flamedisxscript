import numpy as np
import pandas as pd
from tqdm import tqdm
import flamedisx as fd
import json
import argparse

# Create the parser with a description
parser = argparse.ArgumentParser(description='Arguments for toy MC fitting of XENON1T')

# Add arguments with correct syntax for optional arguments and defaults
parser.add_argument('--n_toys', type=int, default=100, help='Number of toys to calculate')
parser.add_argument('--exposure_tonneyear', type=int, default=20, help='Exposure in tonne year')
parser.add_argument('--batch_size', type=int, default=200, help='Batch size for the GPU calculation')
parser.add_argument('--output_file', type=str, default=None, help="Output filename to save results in (must be .json). None doesn't store the results")

# Parse the arguments
args = parser.parse_args()

# Assign arguments to variables
batch_size = args.batch_size
exposure_tonneyear = args.exposure_tonneyear
n_toys = args.n_toys
output_file = args.output_file
fiducial_volume = 5

# Calculate the days to add based on exposure and fiducial volume
days_to_add = np.float64(exposure_tonneyear / fiducial_volume * 365)

# Calculate start and stop times
start_time = pd.Timestamp.now()
stop_time = start_time + pd.DateOffset(days=days_to_add)

# Define the source class and subclasses for ER and WIMP
class MySource:
    t_start = start_time
    t_stop = stop_time

class MyER(MySource, fd.SR0ERSource):
    pass

def make_likelihood(wimp_mass, data=None):
    class MyWIMP(MySource, fd.SR0WIMPSource):
        exposure_tonneyear = exposure_tonneyear
        mw = wimp_mass

    return fd.LogLikelihood(
        sources=dict(er=MyER, wimp=MyWIMP),
        data=data,
        progress=False,
        batch_size=batch_size,
        free_rates=('er', 'wimp'))

# Pre-create likelihoods for a range of masses
masses = np.array([10, 14, 20, 30, 40, 50, 70, 100, 140, 200])
likelihoods = {mw: make_likelihood(mw) for mw in tqdm(masses, desc='Precreating likelihoods...')}

# Simulate and calculate limits for each mass
results = []
for _ in tqdm(range(n_toys), desc='Running simulations'):
    df = likelihoods[10].simulate(wimp_rate_multiplier=0, er_rate_multiplier=np.float(exposure_tonneyear / fiducial_volume))
    for mw in masses:
        ll = likelihoods[mw]
        ll.set_data(df)
        limit = ll.limit('wimp_rate_multiplier')
        results.append({'limit': limit, 'wimp_mass': mw})

# Save results to a JSON file if specified
if output_file and output_file.endswith('json'): 
    with open(output_file, 'w') as file:
        json.dump(results, file) 
